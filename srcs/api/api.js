import axios from 'axios'
import { BASE_URL } from '../constants'

const clientAPI = axios.create({
    baseURL: BASE_URL,
    timeout: 30000,
})

export const getBackgroundImage = async () => {
    const res = await clientAPI.get('/backgroundImg')
    return res.data
}

export const getFriendsData = async () => {
    const res = await clientAPI.get('/friendsData')
    return res.data
}

export const getSounds = async (id) => {
    const res = await clientAPI.get('/sounds', {
        params: {
          id: id
        }
    })
    return res.data
}
