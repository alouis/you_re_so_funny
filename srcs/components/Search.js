import React from 'react';
import { StyleSheet, View, Button, TextInput, TouchableOpacity, Text, ImagePropTypes } from 'react-native';

const Search = (props) => {

    const updateText = (text) => {
        if (text == "" || text.match(/^[A-Za-z]+$/))
            props.onChangeText(text)
    }
    
    return (
        <View style={{flex: 0.2}}>
            <TextInput 
                style={styles.textinput} 
                maxLenght={10}
                value = {props.searchText}
                onChangeText={(text) => updateText(text)}
                placeholder="Asking who ?" 
                //clearButtonMode="while-editing" 
                placeholderTextColor="#E4F1FE"
            />
            {/* <TouchableOpacity style={styles.touchableopacity} onPress={() => {props.onChangeText("")}}><Text>Reset</Text></TouchableOpacity> */}
        </View>
    )

}

//<TouchableOpacity style={styles.touchableopacity} onPress={() => {}}><Text>Entrer</Text></TouchableOpacity>
const styles = StyleSheet.create ({
    textinput: {
        marginTop: 50,
        marginLeft: 5,
        marginRight: 5,
        paddingLeft: 5,
        borderWidth: 1,
        height: 50
    },
    touchableopacity: {
        backgroundColor: 'gray',
        paddingHorizontal: 10,
        paddingVertical: 5,
        alignSelf: 'center'
    }
})

//YELLOW color: '#FFFC5E'//
//BLUE color: '#3477db'



export default Search