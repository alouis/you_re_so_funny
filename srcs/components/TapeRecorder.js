import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { Audio } from "expo-av";
import * as FileSystem from 'expo-file-system';
import { BASE_URL } from '../constants';

export const recordingOptions = {
    isMeteringEnabled: true,
    android: {
        extension: '.m4a',
        outputFormat: Audio.RECORDING_OPTION_ANDROID_OUTPUT_FORMAT_MPEG_4,
        audioEncoder: Audio.RECORDING_OPTION_ANDROID_AUDIO_ENCODER_AAC,
        sampleRate: 44100,
        numberOfChannels: 2,
        bitRate: 128000,
    },
    ios: {
        extension: '.m4a',
        outputFormat: Audio.RECORDING_OPTION_IOS_OUTPUT_FORMAT_MPEG4AAC,
        sampleRate: 44100,
        numberOfChannels: 2,
        bitRate: 128000,
        audioQuality: Audio.RECORDING_OPTION_IOS_AUDIO_QUALITY_MAX,
    },
};

const TapeRecorder = (props) => {
    const [start, setStart] = useState(0)
    const [recording, setRecording] = useState(new Audio.Recording())
    const {friendId: id, disabled, onRecordingToggle} = props

    async function startRecording() {
        onRecordingToggle(true)
        setStart(1);
        try {
            console.log('Requesting permissions..');
            await Audio.requestPermissionsAsync();
            await Audio.setAudioModeAsync({
                allowsRecordingIOS: true,
                playsInSilentModeIOS: true,
            });

            console.log('Starting recording..');
            await recording.prepareToRecordAsync(
                recordingOptions
            );
            console.log(recording.status)
            await recording.startAsync();
            console.log('Recording started');
        } catch (err) {
            console.error('Failed to start recording', err);
        }
    }

    async function stopRecording() {
        console.log('Stopping recording..');
        await recording.stopAndUnloadAsync();
        setStart(0);
        const uri = recording.getURI();
        sendRecording(uri);
        console.log('Recording stopped and stored at', uri);
        setRecording(new Audio.Recording())
        onRecordingToggle(false)
    }

    async function sendRecording(uri) {
        const endpoint = BASE_URL + '/record';
        FileSystem.uploadAsync(endpoint, uri, {
            uploadType: FileSystem.FileSystemUploadType.MULTIPART,
            fieldName: 'sound',
            parameters: {
                friendId: id
            }
        })
    }

    return <View>
        <Button
            title={start ? 'Stop Recording' : 'Start Recording'}
            onPress={start ? stopRecording : startRecording}
            disabled={disabled}
        />
    </View>;
}

export default TapeRecorder