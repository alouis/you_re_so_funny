import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { BASE_URL } from '../constants'

//Equivalent : 
// const FriendItem = (props) => {
//    const { friend, setSelectedFriend } = props
// ...

const FriendItem = ({ friend, setSelectedFriend }) => {
 
    return (
        <View>
            <TouchableOpacity 
            style={styles.global}
            onPress={ () => setSelectedFriend(friend)} 
            ><Image
                style={styles.image}
                source={{uri: BASE_URL + friend.picture_path}}
            />
            <View
            style={styles.content}>
                <Text style={styles.name}>{friend.name}</Text>
                <Text style={styles.text} numberOfLines={2}>{friend.overview}</Text>
                <Text style={styles.date}>Born {friend.date}</Text>
            </View></TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    global: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 5,
        paddingVertical: 5
    },
    image: {
        width: 100,
        height: '100%',
        backgroundColor: '#2589BD'
    },
    content: {
        flex: 4,
        flexDirection: 'column'
    },
    name: {
        flex: 1,
        flexWrap: 'wrap',
        paddingHorizontal: 7,
        textTransform: 'uppercase',
        fontSize: 16,
        fontWeight: 'bold',
        color: '#E4F1FE'
    },
    text: {
        flex: 5,
        paddingHorizontal: 7,
        paddingVertical: 7,
        color: '#FFFC5E',
        fontStyle: 'italic',
        fontSize: 16
    },
    date: {
        flex: 1,
        paddingRight: 5,
        fontSize: 15,
        textAlign: 'right',
        color: 'white'
    }
})

export default FriendItem;