import { setStatusBarNetworkActivityIndicatorVisible } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity, View, Button, StyleSheet, ScrollView } from 'react-native';
import { Audio } from 'expo-av';
import { getSounds } from '../api/api.js'
import { BASE_URL } from '../constants'
import TapeRecorder from './TapeRecorder'


const Player = ({ friend, setSelectedFriend }) => {
    const [playing, setPlaying] = useState(false)
    const [recording, setRecording] = useState(false)
    const [sounds, setSounds] = useState([])

    const loadData = async () => {
        setSounds(await getSounds(friend.id))
    }

    useEffect(() => {
        loadData()
    }, [])

    const play = async (soundPath) => {
        setPlaying(true)
        try {
            const { sound } = await Audio.Sound.createAsync(
                { uri: BASE_URL + soundPath }
            )
            sound.setOnPlaybackStatusUpdate((status) => {
                if (status.didJustFinish == true)
                    setPlaying(false)
            })
            await sound.playAsync()
        } catch (e) {
            setPlaying(false)
        }
    }

    return (
        <View style={styles.global}>
            { sounds.length == 0 ? (
                <Text style={styles.title} >{friend.title} has nothing to say </Text>
            ) : (
                    <>
                        <Text style={styles.title} >{friend.title} talkin' </Text>
                        <ScrollView>
                        {sounds.map((sound, index) =>
                            (
                                <TouchableOpacity
                                    style={styles.playButton}
                                    key={index}
                                    disabled={playing || recording}
                                    onPress={() => { play(sound.sound_path) }}
                                ><Text style={styles.playText} >{sound.sound_name}</Text>
                                </TouchableOpacity>)
                        )}
                        </ScrollView>
                    </>
                )}
            <TapeRecorder
                friendId={friend.id} 
                disabled= {playing}
                onRecordingToggle = { (toggle) => { 
                    if (!toggle){
                        setTimeout(() => loadData(),2000)
                    }
                    setRecording(toggle) 
                }}
            />
            <TouchableOpacity
                onPress={() => setSelectedFriend(null)}
                disabled={playing || recording}
            ><Text style={styles.home} >Home</Text></TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    global: {
        flex: 1,
        marginVertical: 20,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignContent: 'stretch'
    },
    title: {
        padding: 15,
        textAlign: 'center',
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white'
    },
    playButton: {
        borderWidth: 5,
        borderColor: "#F5D76E",
        marginHorizontal: 40,
        borderRadius: 20,
        paddingVertical:10,
        paddingHorizontal: 10,
        marginVertical: 5
    },
    playText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#F5D76E',
        textAlign: 'center',
        
    },
    home: {
        padding: 15,
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white'
    }
})

export default Player