import React from 'react';
import { StyleSheet, View, TextInput, } from 'react-native';

class Mentions extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end', backgroundColor: '#2589BD' }}>
            <View style={[styles.square, { backgroundColor: '#F1F227' }]}/>
                <View style={[styles.square, { backgroundColor: '#2589BD' }]}/>
                <View style={[styles.square, { backgroundColor: '#F1F227' }]}/>
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    square: {
        width: 50,
        height: 50
    }
})

export default Mentions