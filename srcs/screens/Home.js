import React, { useEffect, useState } from 'react';
import { ImageBackground, View, StyleSheet, Text, FlatList } from 'react-native';
import Search from '../components/Search';
import FriendItem from '../components/FriendItem';
import Player from '../components/Player';
import { getBackgroundImage, getFriendsData } from '../api/api.js'

const Home = (props) => {

    const [searchText, setSearchText] = useState("")
    const [selectedFriend, setSelectedFriend] = useState(null)
    const [backgroundImage, setbackgroundImage] = useState(null)
    const [friends, setFriends] = useState([])

    // const loadBackgroundImage = async () => {
    //     const res = await getBackgroundImage()
    //     setbackgroundImage(res)
    // }

    // useEffect(() => {
    //     loadBackgroundImage()
    // }, [])

    // const loadFriends = async () => {
    //     const res = await getFriendsData()
    //     setFriends(res)
    // }

    // useEffect(() => {
    //     loadFriends()
    // }, [])

    // all these equal 2 next functions

    const loadData = async () => {
        setbackgroundImage(await getBackgroundImage())
        setFriends(await getFriendsData())
    }

    useEffect(() => {
        loadData()
    }, [])


    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={{ uri: backgroundImage }} style={styles.image}>
                {selectedFriend == null ? (
                    <React.Fragment>
                        <Search
                            onChangeText={(text) => { setSearchText(text) }}
                            searchText={searchText}
                        />
                        {friends != null && friends.length > 0 ? (
                            <FlatList style={{ paddingTop: 15, flex: 1 }}
                                data={friends.filter((f) => f.name.toLowerCase().includes(searchText.toLowerCase()))}
                                keyExtractor={(item) => item.id.toString()}
                                renderItem={({ item }) => <FriendItem friend={item} setSelectedFriend={setSelectedFriend} />}
                            />
                        ) : ( <Text>Loading</Text>)
                        }
                    </React.Fragment>
                ) : (
                        <Player
                            friend={selectedFriend}
                            setSelectedFriend={setSelectedFriend}
                        />
                    )}
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        height: '110%',
        resizeMode: "cover",
        justifyContent: "center"
    }
})

export default Home