import React from 'react';
import { ImageBackground, View, StyleSheet, Text, FlatList } from 'react-native';
import Search from '../components/Search';
import friends from '../helpers/friendsData';
import FriendItem from '../components/FriendItem';

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = { 
            searchText : ""
        }
    }
    
    onChangeText (text) {
        this.setState({searchText: text})
        console.log(this.state.searchText)
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <ImageBackground source={{ uri: "http://pngimg.com/uploads/mars_planet/mars_planet_PNG23.png" }} style={styles.image}>
                    <Search 
                        onChangeText = {(text) => {  this.onChangeText(text)}}
                    />
                    <FlatList style={{paddingTop: 15}}
                    data={friends.filter( (f) => f.name.toLowerCase().includes(this.state.searchText.toLowerCase()) )}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <FriendItem friend={item}/>}
                    />
                    {/* <Mentions/> */}
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    image: {
        flex: 1,
        height: '110%',
        resizeMode: "cover",
        justifyContent: "center"
    }
})

export default Home