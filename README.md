# You're so funny
The mobile app your friends dream of


## Get started 

### Best practices :white_check_mark:
 * Write understandable commits
 * Put anything you found useful here
 * Comment when modifying someone else's code
 * Try explaining it to the rubber duck (except for undefined in js, they're a pain in the ass, _ask Morgan_)

### Learn the basics :books:
 * [Javascript](https://openclassrooms.com/en/courses/5664271-learn-programming-with-javascript)
 * [Native React (FR)](https://openclassrooms.com/en/courses/4902061-developpez-une-application-mobile-react-native/4902068-decouvrez-le-developpement-mobile-actuel)


## Very first steps :hatched_chick:

```sh
# install node (localhost server and libraries), expo (simulator) and watchman (updater)
brew install node
npm install -g expo-cli
brew install watchman

# build everything needed to developp a mobile app
expo init 
#name app
#select blank

# if the ressources already exist, you just need to install dependancies (they're all listed with the right version in package.json)
yarn install

# start the simulation
expo start 
```

Scan QR code with your phone (install expo app and create an account) or choose your simulator on the left panel of the browser window expo just opened.
Have fun !


## Ressources :bulb:

* [Tips](https://medium.com/@saumya.ranjan/how-to-write-a-readme-md-file-markdown-file-20cb7cbcd6f) and [emojis](https://gist.github.com/ricealexander/ae8b8cddc3939d6ba212f953701f53e6) for a beautiful README.md

### Javascript
 * Good Javascript documentation [here](https://javascript.info) and [there](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
 * [W3School Tutorials](https://www.w3schools.com/js/DEFAULT.asp)
 * [A comprehensive guide to Javascript Patterns](https://www.toptal.com/javascript/comprehensive-guide-javascript-design-patterns)
 * [Flexbox in CSS are the same (except for small syntax differences)](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

### React Native
 * [React Native Documentation](https://devdocs.io/react_native/)
 * [Styling CheatSheet](https://github.com/vhpoet/react-native-styling-cheat-sheet)
 * [Native React Components' **props**](https://reactnative.dev/docs/text#props)
 * [React Hooks Cheat Sheet](https://blog.logrocket.com/react-hooks-cheat-sheet-unlock-solutions-to-common-problems-af4caf699e70/)

### Expo
 * [Audio](https://docs.expo.io/versions/latest/sdk/audio/)
 * [AV](https://docs.expo.dev/versions/latest/sdk/av/)
 * [FileSystem](https://docs.expo.dev/versions/latest/sdk/filesystem/)
 * [Uploading Audio](https://dev.to/jillianntish/uploading-audio-to-cloudinary-in-expo-2hl6)

### Design
* [Which colors to choose](https://coolors.co)
* [Check color combinations](http://colorsafe.co)
* [Templates from Figma](https://www.figmafinder.com)
* [Native Base Sketch Templates](https://nativebase.io/sketch-template?ref=producthunt)


## ERRORS :wrench:
Write here errors and their solution

* Live Reloading issue: if nothing changes after code was modified, press **ctrl + R** or **ctrl + D** and select **Reload** in Debug menu
* [Recording works only a few times](https://github.com/expo/expo/issues/1709): Failed to start recording, [Error: This Recording object is done recording; you must make a new one.]
* [Recorded audio on Android won't play on iOS](https://github.com/expo/expo/issues/11952)
